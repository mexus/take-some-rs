use TakeSome;

impl<T> TakeSome for Vec<T> {
    type Item = T;

    /// Pops up the last element of the vector.
    fn take_some(&mut self) -> Option<T> {
        self.pop()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_vec() {
        let mut empty: Vec<String> = Vec::new();
        assert_eq!(None, empty.take_some());

        let mut one_element: Vec<_> = vec!["check".to_string()];
        assert_eq!(Some("check".into()), one_element.take_some());
        assert!(one_element.is_empty());

        let mut vec: Vec<_> = vec!["check".to_string(), "lol".into(), "wut".into()];
        assert!(vec.take_some().is_some());
        assert_eq!(2, vec.len());
    }
}
