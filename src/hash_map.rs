use std::collections::HashMap;
use std::hash::{BuildHasher, Hash};
use TakeSome;

impl<K, V, S> TakeSome for HashMap<K, V, S>
where
    K: Eq + Hash,
    S: BuildHasher,
{
    type Item = (K, V);

    /// `unsafe` under the cover!
    fn take_some(&mut self) -> Option<(K, V)> {
        // If the key corresponds to the "first" element (the one we store separately), we have
        // to take a "random" element from the map (`self.rest`) and put is as the "first".
        let temp_key: *const K = match self.keys().next() {
            Some(x) => x,
            None => return None,
        };
        // We need that unsafe magic because we are going to remove an element a reference to
        // which we kinda hold.
        let temp_key: &K = unsafe { &*temp_key };
        Some(
            self.remove_entry(temp_key)
                .expect("We've just pulled a key from the map"),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::iter::once;

    #[test]
    fn check_hash_map() {
        let mut empty: HashMap<String, String> = HashMap::new();
        assert_eq!(None, empty.take_some());

        let mut one_element: HashMap<_, _> =
            once(("check".to_string(), "checked".to_string())).collect();
        assert_eq!(
            Some(("check".into(), "checked".into())),
            one_element.take_some()
        );
        assert!(one_element.is_empty());

        let mut map: HashMap<_, _> = vec![
            ("check".to_string(), "lol".to_string()),
            ("wut".into(), "ahaha".into()),
            ("123123".into(), "....".into()),
        ].into_iter()
            .collect();
        assert!(map.take_some().is_some());
        assert_eq!(2, map.len());
    }
}
